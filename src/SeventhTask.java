import java.util.HashMap;
import java.util.Map;

public class SeventhTask {

    public static void main(String[] args) {

        String[] arrayBeforeSwap = new String[]{"ab", "ac"};
        String[] arrayBeforeSwap2 = new String[]{"ax", "bx", "cx", "cy", "by", "ay", "aaa", "azz"};

        for (String s : allSwap(arrayBeforeSwap)) {
            System.out.print(s + "; ");
        }

        System.out.println();

        for (String s : allSwap(arrayBeforeSwap2)) {
            System.out.print(s + "; ");
        }
    }

    private static String[] allSwap(String[] array) {
        Map<Character, Integer> map = new HashMap<Character, Integer>();
        String[] swapedArray = new String[array.length]; //задаем такую же длинну, чтобы не выпала ошибка

        for (int i = 0; i < array.length; i++) {
            char c = array[i].charAt(0);
            if (map.containsValue(c)) {
                int parametr = map.get(c);
                map.remove(c);
                swapedArray[i] = swapedArray[parametr];
                swapedArray[parametr] = array[i];
            } else {
                swapedArray[i] = array[i];
                map.put(c, i);
            }
        }
        return swapedArray;
    }
}
