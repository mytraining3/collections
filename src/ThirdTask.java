import java.util.ArrayList;
import java.util.List;

public class ThirdTask {

    public static void main(String[] args) {
        List<Integer>  list = new ArrayList<>();
        list.add(3);
        list.add(1);
        list.add(4);
        System.out.println(square56(list));
    }

    private static List<Integer> square56(List<Integer> nums) {
        nums.replaceAll(n -> (int) Math.pow(n, 2) + 10);
        nums.removeIf(n ->
                Integer.toString(n).charAt(Integer.toString(n).length()-1) == '5'
                        || Integer.toString(n).charAt(Integer.toString(n).length()-1) == '6');
        return nums;
    }
}
