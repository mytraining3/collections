import java.util.ArrayList;
import java.util.List;

public class SecondTask {

    public static void main(String[] args) {

        List<Integer> list = new ArrayList<>();
        list.add(12);
        list.add(13);
        list.add(19);
        list.add(20);

        list = returnWithout(list);
        System.out.println(list);

        System.out.println();
        List<Integer> list2 = new ArrayList<>();
        list2.add(1);
        list2.add(14);
        list2.add(1);

        list2 = returnWithout(list2);
        System.out.println(list2);
    }

    private static List<Integer> returnWithout(List<Integer> list) {
         list.removeIf(i-> i>=13 && i<=19);
        return list;
    }
}
