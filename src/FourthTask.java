import java.util.ArrayList;
import java.util.List;

public class FourthTask {

    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("a");
        list.add("b");
        list.add("cy");

        list= noYY(list);
        System.out.println(list);

        //list = noYY2(list);
    }

    private static List<String> noYY(List<String> strings) {
        strings.replaceAll(s -> s + "y");
        strings.removeIf(s -> s.contains("yy"));
        return strings;
    }

    //without lambdas
    private static List<String> noYY2(List<String> strings){
        for (String s : strings) {
            s = s + "y";
            if (s.contains("yy"))
                strings.remove(s);
        }
        return strings;
    }
}
