import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class FirstTask {

    public static void main(String[] args) {

        ArrayList<Integer> firstList = new ArrayList<>();
        LinkedList<Integer> secondList = new LinkedList<>();

        long starttime = System.currentTimeMillis();
        addElements(1000000, firstList);
        long endtime = System.currentTimeMillis();
        System.out.println("time of adding into arrayList : " + (endtime - starttime));
        //выполняется бысрее за О(1) тк известен индекс последнего элемента
        //времяя вставки и удаление последнего элемента является постоянным(константным)

        starttime = System.currentTimeMillis();
        addElements(1000000, secondList);
        endtime = System.currentTimeMillis();
        System.out.println("time of adding into linkedList : " + (endtime - starttime));
        //выполняется медленнее тк необходимо найти последний узел и после вставить элемент

        System.out.println();
        starttime = System.currentTimeMillis();
        System.out.println(selectElement(100000, firstList));
        endtime = System.currentTimeMillis();
        System.out.println("time of adding into linkedList : " + (endtime - starttime));

        System.out.println();
        starttime = System.currentTimeMillis();
        System.out.println(selectElement(100000, secondList));
        endtime = System.currentTimeMillis();
        System.out.println("time of adding into linkedList : " + (endtime - starttime));
    }

    private static void addElements(int count, List<Integer> list) {
        Random random = new Random();
        for (int i = 0; i < count; i++) {
            list.add(random.nextInt());
        }
    }

    private static int selectElement(int howMuchTimeSelect, List<Integer> list) {
        int element = 0;
        for (int i = 0; i < howMuchTimeSelect; i++) {
            element = list.get((int) Math.random());
        }
        return element;
    }
}
