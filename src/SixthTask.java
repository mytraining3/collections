import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SixthTask {

    public static void main(String[] args) {
        String[] strings = new String[]{"a", "b", "a", "c", "a", "d", "a"};
        System.out.println(wordAppend(strings));
    }

    private static String wordAppend(String[] strings) {
        Map<String, Integer> map = new HashMap<String, Integer>();
        String result = "";
        for (String s : strings) {
            int i = 0;
            if (map.containsKey(s)) {
                i = map.get(s) + 1;
                map.put(s, i);
                if (i % 2 == 0) {
                    result = result + s;
                }
            } else {
                map.put(s, ++i);
            }
        }
        return result;
    }
}
