import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FifthTask {

    public static void main(String[] args) {

        ArrayList<String> list = new ArrayList<>();
        list.add("a");
        list.add("b");
        list.add("a");
        list.add("c");
        list.add("b");
        Map map = wordMultiple(list);
        System.out.println(map);

    }

    private static Map<String, Boolean> wordMultiple(List<String> strings) {
        Map map = new HashMap();
        for (String s : strings) {
            if (map.containsKey(s)) // if contains it appears the second or more times in array
                map.put(s, true);
            else
                map.put(s, false);
        }
        return map;
    }
}
