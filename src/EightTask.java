import java.util.*;
import java.util.stream.IntStream;

public class EightTask {
    public static void main(String[] args) {

        LinkedList<Integer> lst = new LinkedList<>();
        lst.add(20);
        lst.add(37);
        lst.add(20);
        lst.add(21);
        System.out.println(deleteNth(lst, 1));

        deleteNth(new int[]{1, 1, 3, 3, 7, 2, 2, 2, 2}, 3);
        for (int i : deleteNth(new int[]{1, 1, 3, 3, 7, 2, 2, 2, 2}, 3)) {
            System.out.print(i);
        }

    }

    private static int[] deleteNth(int[] lst, int n) {
        Map<Integer, Integer> map = new HashMap<Integer, Integer>();

        return IntStream
                .of(lst)
                .filter(l -> map.merge(l, 1, Integer::sum) < n )
                .toArray();
    }

    private static LinkedList<Integer> deleteNth(LinkedList<Integer> lst, int n) {
        Map<Integer, Integer> map = new HashMap<Integer, Integer>();
        for (int i=0; i< lst.size(); i++) {
            map.merge(lst.get(i), 1, (oldVal, newVal) -> oldVal + newVal);
            if(map.get(lst.get(i)) > n)
                lst.remove(i);
        }
        return lst;
    }
}
